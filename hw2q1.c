#include <stdio.h>

int main() {
    char c;
    int num = 0;
    while ((c = getchar()) != '\n') {
        num = (10 * num) + (c - '0');
    }
    int rearranged_num[100];
    int i = 0;
    while(num > 0){
        rearranged_num[i] = num % 10;
        num = num / 10;
        i++;
    }

    for(int j = 0; j < i; j++) {
        for (int k = 0; k < i; k++) {
            if (rearranged_num[j] > rearranged_num[k]) {
                int temp = rearranged_num[j];
                rearranged_num[j] = rearranged_num[k];
                rearranged_num[k] = temp;
            }
        }
    }
    int smallest = 0;
    for(int j = 0; j < i; j++){
        smallest = (smallest * 10) + rearranged_num[i-j-1];
    }
    printf("%d", smallest);

    return 0;
}
