#include <stdio.h>

int main() {

    char string[100];
    char c;
    int k = 0;
    while((c = getchar()) != '\n'){
        string[k] = c;
        k++;
    }
    char temp;
    int i, j;
    for (i = 0; i < k-1; i++) {
        for (j = i + 1; j < k; j++) {
            if (string[i] > string[j]) {
                temp = string[i];
                string[i] = string[j];
                string[j] = temp;
            }
        }
    }

    printf("%s", string);
    return 0;
}
