#include <stdio.h>

int readword(char word[]){
    char c;
    int i = 0;
    while((c=getchar()) != ' ' && c != '\t' && c != '\n'){
        if (c >= 'A' && c <= 'Z') {
            c = c + 32;
        }
        word[i++] = c;
    }
    word[i] = '\0';
    return i;
}

int find(char word1[], char word2[]){
    int i, j, flag;

    for(i=0; word1[i] != '\0'; i++){
        if(word1[i] == word2[0]){
            flag = 1;
            for(j=0; word2[j]!='\0'; j++){
                if(word2[j] != word1[i+j] || word1[i+j] == '\0'){
                    flag = 0;
                    break;
                }
            }
            if(flag == 1){
                return 1;
            }
        }
    }
}


int main() {
    char word1[100],word2[100];
    readword(word1);
    readword(word2);
    printf("%d", find(word1,word2));

    return 0;
}
