#include <stdio.h>

int main( ){
    char line[1000];
    char c;
    int i = 0;
    while(1) {
        if ((c = getchar()) != '\n' && c != ' ' && c != '\t') {
            line[i] = c;
            i++;
            continue;
        }
        if (c == '\n') {
            if ((c = getchar()) == '\n') {
                break;
            }
            if (c != '\n' && c != ' ' && c != '\t') {
                line[i] = '\n';
                line[i + 1] = c;
                i = i + 2;
            }
        }
    }
    line[i] = '\0';
    printf("%s", line);
    return 0;
}
