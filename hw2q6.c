#include <stdio.h>

int mystrcmp(char word1[], char word2[]){
    int c = 0;
    while (word1[c] == word2[c]) {
        if (word1[c] == '\0' || word2[c] == '\0') {
            break;
        }
        c++;
    }
    if (word1[c] == '\0' && word2[c] == '\0')
        return 0;
    else
        return -1;
}

int main() {
    char word1[1000] = "hello";
    char word2[1000] = "hello";
    printf("%d", mystrcmp(word1, word2));
    return 0;
}
