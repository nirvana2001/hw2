#include <stdio.h>

int rotation(int len, int arr[], int k){

    while(k > 0) {
        int temp = arr[len-1];
        int i;
        for (i = len-1 ; i>=1; i--) {
            arr[i] = arr[i-1];
        }
        arr[0] = temp;
        k--;
    }
    for(int j=0; j<len ; j++){
        printf("%d ",arr[j]);
    }
    return 0;
}

int main() {
    int arr[] = {1, 5, 4, 7, 2, 9};
    int len = sizeof(arr)/ sizeof(arr[0]);
    arr[len] = '\0';
    rotation(len, arr , 3);

    return 0;
}
